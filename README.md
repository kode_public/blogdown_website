# Blogdown

Il sito si basa sul package R blogdown. [Qui](https://bookdown.org/yihui/blogdown/) la guida iniziale per capirne le basi. Il package di per se è semplice, e permette di creare praticamente qualsiasi tipo di sito grazie ai temi disponibili.

[Qui](https://themes.gohugo.io/) c'è una raccolta infinita di temi, anche se sul sito di blogdown avvertono che non tutti sono stati testati con questo framework, quindi sono da scegliere con attenzione. Io ho scelto il tema Academic, che è consigliato anche nella guida di blogdown, e mi sembrava abbastanza completo.

## Tema Academic

Il tema ha tantissime opzioni, ed è abbastanza complesso da configurare. Riporto qui alcuni link su risorse che potrebbero essere utili:
* homepage del tema [link](https://sourcethemes.com/academic)
* repo github del tema [link](https://github.com/gcushen/hugo-academic)
* mini tutorial [link](https://andreaczhang.rbind.io/post/my-1st-blogpost/)

## Homepage

La home è una pagina particolare, che viene popolata da widget. Per vedere quali widget sono disponibili conviene frugare nel progetto demo del tema (si può scaricare ad esempio creando un nuovo sito tramite l'addin di Rstudio e impostando come tema Academic).

Ogni widget è definito da un file *.md, che si trova nella cartella content/home. Questi file contengono una serie di parametri che è possibile configurare.

## Come creare un post

Se si sta usando Rstudio, è disponibile un addin che permette di creare un nuovo post in maniera facilitata, inserendo alcune informazioni tramite un'interfaccia shiny. Questa modalità crea un file markdown nella directory scelta con una serie di parametri già impostati nello YAML.

Alternativamente è possibile:
* aggiungere il file (markdown o Rmd) nella directory desiderata (ad esempio, content/infographic per le infografiche) e aggiungere a mano eventuali altre opzioni nello YAML. Questa opzione sarebbe da usare solo per i post più semplici a mio avviso, ovvero quelli che sono composti da un solo file e non hanno dipendenze da dati o immagini.
* aggiungere una directory con il nome del post desiderato, e dentro la directory inserire il file markdown, rinominandolo come index.md (o Rmd). In questo modo si possono mettere nella cartella anche tutti i file del post e eventuali immagini di anteprima (featured.png).

## Preview e Build del sito

### Preview

Per vedere l'anteprima del sito, usare `blogdown::serve_site()`. 

### Build

Il comando `blogdown::build_site` si usa per costruire il sito da pubblicare online. Il comando fa essenzialmente due cose:

* compilazione di **tutti** i markdown e generazione degli html
* build del sito tramite hugo

Se si vuole evitare la ricompilazione di tutti i markdown, si può in alternativa eseguire i due step separatamente:

* compilare solo i markdown che si desidera con 

```r
blogdown::build_dir("content/infographic")
```

* fare la build del sito con

```r
blogdown::hugo_build()
```

Nota bene che la ricompilazione dei markdown è obbligata se cambiano nomi e riferimenti, altrimenti i link del sito non funzioneranno più.