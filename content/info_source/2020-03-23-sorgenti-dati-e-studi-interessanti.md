---
title: Sorgenti dati e studi interessanti
author: KODE srl
date: '2020-03-23'
slug: sorgenti-dati-e-studi-interessanti
categories: []
tags: []
subtitle: ''
summary: ''
authors: []
lastmod: '2020-03-23T15:39:58+01:00'
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: no
projects: []
---


# Introduzione

In questa pagine troverete link a sorgenti dati, a studi che mi sono piaciuti o a articoli che reputo interessanti.
Chi legge tenga presente che questo lavoro non ha nessuna pretesa di scientificità, non è revisionato da altri e contiene solo pareri ed opinioni personali.

# Dashboard con informazioni pronte all'uso
* Innanzi tutto un [infografica](https://lab24.ilsole24ore.com/coronavirus/) del [lab24](https://lab24.ilsole24ore.com/home/) del *Sole24ore*, che riporta una sintesi dei dati nazionali.
* [COVID-19 / I dati di regioni e province](https://covid-19.netseven.it/regioni-province). Fatto da [Net7](https://www.netseven.it/), lavorod semplice, intuitivo e informativo.
* Il Dipartimento della Protezione Civile ha elaborato [un cruscotto geografico interattivo](http://arcg.is/C1unv) (anche in versione [mobile](http://arcg.is/081a51)).

# Dati
* I dati in formato machine readable pubblicati giornalmente dal Dipartimento della Protezione Civile sono qui: [COVID-19 Italia - Monitoraggio situazione](https://github.com/pcm-dpc/COVID-19)
* [Questa fonte](https://en.wikipedia.org/wiki/Template:2019–20_coronavirus_pandemic_data/Italy_medical_cases) dati (wikipedia) riporta il dettaglio giornaliero, per regione. I dati non sono esattamente machine readable ma sono comunque interessanti.

# Analisi
* Il portale dell'epidemiologia per la sanità pubblica (Epicentro)
a cura dell'[Istituto superiore di sanità](https://www.epicentro.iss.it/#itoss) produce un'[analisi dei dati](https://www.epicentro.iss.it/coronavirus/), descrittiva, che mi sembra molto interesante e ben fatta. In particolar modo due sono gli approfondimenti interessanti: 
  * [Caratteristiche dei pazienti deceduti positivi a COVID-19 in Italia](https://www.epicentro.iss.it/coronavirus/sars-cov-2-decessi-italia), 
  * [Sorveglianza integrata COVID-19: i principali dati nazionali](https://www.epicentro.iss.it/coronavirus/sars-cov-2-sorveglianza-dati). Da questa pagina si può visualizzare l'infografica che mostra non solo l'andamento dei tampo  ma anche quello dela prima manifestazione dei sintomi.
*[CoVsta_IT](https://covstat.it) è un progetto molto interesante perchè cerca di psiegare in modo chiaro, anche ai non addetti ai lavori, come stanno andado le cose.
* In [questo articolo del Washington Post](https://www.washingtonpost.com/graphics/2020/world/corona-simulator/) [Harry Stevens](https://www.washingtonpost.com/people/harry-stevens/) ci spiega, senza numeri e matematica,come avviene il contagio e quali possano essere gli effetti delle varie strategie di contenimento.
* [COVID-19 Mobility Monitoring project](https://covid19mm.github.io/in-progress/2020/03/13/first-report-assessment.html) è un lavoro fatto e segnalato su twitter da [Michele Tizzoni](https://twitter.com/mtizzoni), basato sui dati di mobilità. Interessante.
* [Algebris investmentes](https://www.algebris.com/policy-research-forum/blog/covid-19-facts/#) sta portando avanti, ad opera di [Silvia Merler](https://twitter.com/SMerler), con aggiornamenti giornalieri, un confronto tra le nazioni europee e, ovviamente, un confronto tra Italia e Cina. E' un lavoro che sta avendo un ampio riscontro mediatico.
* Se invece poi volete un qualcosa di veramente pessimista, leggete [questo post](https://threadreaderapp.com/thread/1238837158007447558.html#) di [Francois Balloux](https://twitter.com/BallouxFrancois)...che purtroppo non sembra essere proprio uno sprovveduto...
* [Analisi interessante ed esaustiva](http://nrg.cs.ucl.ac.uk/mjh/covid19/) sul confronto tra le traiettorie di difusione nei vari stati. Autore: [Mark J Handley](https://twitter.com/MarkJHandley?s=20)

# Analisi con codice condiviso
* [COVID-19 epidemiology with R](https://rviews.rstudio.com/2020/03/05/covid-19-epidemiology-with-r/) è un lavoro fatto 2020-03-05 da Tim Churches. Tim Churches è Senior Research Fellow presso la UNSW Medicine South Western Sydney Clinical School e Health Data Scintist presso l'Ingham Institute for Applied Medical Research, anch'esso situato a Sydney. Cita le sorgenti dati e pubblica il codice della analisi. Tra le varie cose, a me che non ho un background in epidemiologia, è sembrato interessante il calcolo dell'*instantaneous effective reproduction ratio* e i vari fit della progressione della malattia.
* Sempre restando in ambito R mi è stato segnalato questo lavoro: [COVID-19 Data Analysis with Tidyverse and Ggplot2 - China](https://78462f86-a-e2d7344e-s-sites.googlegroups.com/a/rdatamining.com/www/docs/Coronavirus-data-analysis-china.pdf?attachauth=ANoY7co87ZO2g5z0rWR6Owt2h4sKzwsjaOGb9_kDiwUWG6yuv--G7u9_MasAlloOw_pFXe9JbBNsNTER7V_WhzHU8YphMXmewCPB8vlxLe0XykOjP6dFDoEN772LQNYl4Of4--3A4INhBDJr3v6QQxS39Lk3dJCyert4oGlLWZK45KCEWzifxV30HoNnUziOMynPVr1xIsXsxn48N9MJkF7cve3qp4NkPgsqz3tghINrt97JnjS6mAg%3D&attredirects=0). Anche in questo caso è possibile utilizzare il codice per fare analisi sui propri dati.

# Articoli e lavori scientifici

* Cito direttamente dalle conclusioni riportate in [questo articolo](https://cmr.asm.org/content/cmr/20/4/660.full.pdf) del __2007__: "La presenza di un vasto archivio di virus simili a SARS-CoV nei pipistrelli, insieme alla cultura del consumo di mammiferi esotici nella Cina meridionale, è una bomba a orologeria". _Severe Acute Respiratory Syndrome Coronavirus as an Agent of Emerging and Reemerging Infection._ Vincent C. C. Cheng, Susanna K. P. Lau, Patrick C. Y. Woo, and Kwok Yung Yuen, _Clinical Microbiology Reviews_, Oct. 2007, p. 660–694 Vol. 20, No. 4 doi:10.1128/CMR.00023-07. 
* [Perché l’inquinamento da Pm10 può agevolare la diffusione del virus.](https://www.ilsole24ore.com/art/l-inquinamento-particolato-ha-agevolato-diffusione-coronavirus-ADCbb0D) La correlazione evidenziata dall’analisi dei dati delle Arpa congiunta ai numero dei contagiati: il Pm10 agirebbe da vettore del virus di [M.Cristina Ceresa](https://www.linkedin.com/in/cristinaceresa/)
* No, mi spiace, l'età non c'entra nulla. Ci sono zone in Italai dove l'età media è molto più alta di quelle che si ha nelle zone più colpite eppure lì il virus non è così micidiale: [Contagi e anziani, ecco dove il coronavirus rischia di provocare più vittime](https://www.infodata.ilsole24ore.com/2020/03/19/43483/) di [Davide Mancino](https://twitter.com/davidemancino1?s=20)
* [Aerosol and Surface Stability of SARS-CoV-2 as Compared with SARS-CoV-1](https://www.nejm.org/doi/10.1056/NEJMc2004973). Lettera pubblicata il 17 marzo 2020 sul New England Journal of Medicine (doi: 10.1056/NEJMc2004973). Van Doremalen, Bushmaker e Morris espondo i loro risultati relativamente a quanto tempo resiste il virus in aria e sulle superici.

