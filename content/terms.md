---
commentable: false
date: "2018-06-28T00:00:00+01:00"
draft: true
editable: false
header:
  caption: ""
  image: ""
share: false
title: Terms
---


__Posso scaricare i dati?__

Si. Mi farebbe piacere però se citassi la fonte, visto il lavoro di raccolta, pulizia e strutturazione.

__Posso usare i grafici?__

Sì, sono sotto licenza CC0 "No rights reserved". Puoi usarli per qualsiasi scopo, ma preferirei utilizzassi il link al sito, perché le spiegazioni sono importanti.
