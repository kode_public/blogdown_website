---
title: Distribuzione territoriale e temporale casi USL Toscana Nord Ovest
author: KODE srl
date: '2020-03-20'
slug: distribuzione-territoriale-e-temporale-casi-usl-toscana-nord-ovest
categories:
  - infographics
  - maps
tags:
  - dataviz
  - map
subtitle: ''
summary: ''
authors: []
draft: true
lastmod: '2020-03-20T17:42:16+01:00'
featured: no
image:
  caption: ''
  focal_point: ''
  preview_only: yes
projects: []
---

La mappa ([qui](http://app.kode-datacenter.net:14000/covid-19/) in una visualizzazione più usabile) illustra la distribuzione territoriale dei casi di COVID19 rilevati nella USL nord-ovest della Toscana durante il periodo 27 febbraio - 15 marzo 2020. 

Alla mappa è stato applicato un layer con il fine di evidenziare la Toscana e le sue usl.
La rappresentazione è composta da una serie di cerchi la cui dimensione e colore sono proporzionali al numero di contagiati individuati per comune.

Passando il mouse sopra il singolo cerchio è possibile:
- visualizzare in un popup il conteggio dei casi di COVID19 suddivisi in "decessi", "terapia intensiva", "ricovero", "domicilio" e "dimessi"; 
- visualizzare in un grafico a piramide la distribuzione di COVID19  per sesso e per età.
Un ulteriore grafico riporta i dati cumulativi e l'andamento giornaliero dei casi di COVID19: uno slider permette di selezionare un range di date che filtrano di conseguenza i dati della mappa.

Le tecnologie utilizzate per sviluppare questo tipo di visualizzazione sono:
- R (elaborazione dei dati)
- mapbox;
- dcjs
- crossfilter
- highcharts
- javascript

<iframe style="width:100%;height:800px" src=http://app.kode-datacenter.net:14000/covid-19/>
</iframe>
