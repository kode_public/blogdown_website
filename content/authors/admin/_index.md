---
authors:
- admin
email: ""
name: KODE team
bio: "https://www.kode-solutions.net"
organizations:
- name: KODE srl
  url: "https://www.kode-solutions.net"
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'
superuser: true
user_groups:
- Researchers
- Visitors
---

Kode, attiva dal 2012, è stata una delle prime società di consulenza scientifica operanti nel campo della data science. Il nostro core business si focalizza sullo sviluppo e l’erogazione di soluzioni di intelligenza artificiale in ambito Industry 4.0 e Healthcare.

Crediamo nella forza della multidisciplinarietà come mix di competenze, esperienze e provenienze capaci di leggere la realtà che ci circonda da diversi punti di vista. In Kode guardiamo ed analizziamo i dati attraverso gli occhi di informatici, statistici, chimici, psicologi, ormai esperti di data science e di computer science.

La passione per i dati e la formazione continua sulle più avanzate tecniche di intelligenza artificiale, tecnologie trasversali e specifiche novità dei settori in cui operiamo, ci rendono entusiasti promotori dell’innovazione e del progresso tecnologico con un approccio socialmente consapevole e sensibile verso le tematiche ambientali e le persone.